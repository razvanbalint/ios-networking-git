import XCTest

import CVUNetworkingTests

var tests = [XCTestCaseEntry]()
tests += CVUNetworkingTests.allTests()
XCTMain(tests)
