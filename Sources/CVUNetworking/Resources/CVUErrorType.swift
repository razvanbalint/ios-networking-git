//
//  CVUErrorType.swift
//  CVU.FirstProject
//
//  Created by Razvan Balint on 10/12/2019.
//  Copyright © 2019 CVU Intelligence. All rights reserved.
//

import Foundation

public enum CVUErrorType: Error {
    case unknown
    case invalidResourceId
    case jsonSerializationFailed
    case jsonMappingFailed
    case noResponse
    case invalidToken
    case noInternetConnection
    
    var localizedDescription: String {
        switch self {
        case .unknown:
            return "Unknown error occured"
        case .invalidResourceId:
            return "Incorrect resource identifier"
        case .jsonSerializationFailed:
            return "Incorrect JSON data format"
        case .jsonMappingFailed:
            return "Incorrect server response format"
        case .noResponse:
            return "Server could not be reached"
        case .invalidToken:
            return "You are not authorized"
        case .noInternetConnection:
            return "No internet connection"
        }
    }
}
