//
//  CVUResource.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation


public protocol CVUResource: CVUCodable, Equatable {
    associatedtype T: Equatable
    
    var id: T { get }
}

extension CVUResource {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    var idValue: String? {
        switch id {
        case is Int:
            return (id as! Int).description
        case is String:
            return (id as! String)
        case is UUID:
            return (id as!  UUID).uuidString
        default:
            return nil
        }
    }
}
