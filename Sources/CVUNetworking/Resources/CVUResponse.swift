//
//  CVUResponse.swift
//  CVU.FirstProject
//
//  Created by Razvan Balint on 18/01/2019.
//  Copyright © 2019 CVU Intelligence. All rights reserved.
//

import Foundation


public enum CVUResponse<T: CVUDecodable, E: CVUDecodable> {
    case success(statusCode: Int, data: T?, headers: [AnyHashable: Any])
    case error(statusCode: Int?, error: E?, headers: [AnyHashable: Any]?)
}

public enum CVURawResponse {
    case success(statusCode: Int, data: Data?, headers: [AnyHashable: Any])
    case error(statusCode: Int?, error: Data?, headers: [AnyHashable: Any]?)
    
    public static func localError(_ type: CVUErrorType) -> CVURawResponse {
        let error = try? CVUError(type: type).encoded()
        return CVURawResponse.error(statusCode: nil, error: error, headers: nil)
    }
}


public extension CVUResponse {
    var statusCode: Int? {
        switch self {
        case .success(let statusCode, _, _):
            return statusCode
        case .error(let statusCode, _, _):
            return statusCode
        }
    }
    
    var data: T? {
        switch self {
        case .success(_, let data, _):
            return data
        case .error(_, _, _):
            return nil
        }
    }
    
    var error: E? {
        switch self {
        case .success(_, _, _):
            return nil
        case .error(_, let error, _):
            return error
        }
    }
    
    var headers: [AnyHashable: Any]? {
        switch self {
        case .success(_, _, let headers):
            return headers
        case .error(_, _, let headers):
            return headers
        }
    }
    
    var isSuccess: Bool {
        switch self {
        case .success(_, _, _):
            return true
        case .error(_, _, _):
            return false
        }
    }
}

public extension CVURawResponse {
    var statusCode: Int? {
        switch self {
        case .success(let statusCode, _, _):
            return statusCode
        case .error(let statusCode, _, _):
            return statusCode
        }
    }
    
    var data: Data? {
        switch self {
        case .success(_, let data, _):
            return data
        case .error(_, _, _):
            return nil
        }
    }
    
    var error: Data? {
        switch self {
        case .success(_, _, _):
            return nil
        case .error(_, let error, _):
            return error
        }
    }
    
    var headers: [AnyHashable: Any]? {
        switch self {
        case .success(_, _, let headers):
            return headers
        case .error(_, _, let headers):
            return headers
        }
    }
    
    var isSuccess: Bool {
        switch self {
        case .success(_, _, _):
            return true
        case .error(_, _, _):
            return false
        }
    }
}
