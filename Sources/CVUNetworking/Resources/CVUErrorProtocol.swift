//
//  CVUNetworkingLocalizedError.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

public protocol CVUErrorProtocol: CVUDecodable {
    var message: String { get set }
}

public struct CVUError: CVUErrorProtocol, CVUEncodable {
    public var message: String
    
    init(message: String) {
        self.message = message
    }
    
    init(type: CVUErrorType) {
        self.message = type.localizedDescription
    }
}
