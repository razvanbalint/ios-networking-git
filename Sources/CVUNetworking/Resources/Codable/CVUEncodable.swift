//
//  CVUEncodable.swift
//  Healthcare
//
//  Created by Razvan Balint on 03/04/2020.
//  Copyright © 2020 Razvan Balint. All rights reserved.
//

import Foundation

public protocol CVUEncodable: Encodable {
    var dateFormat: String { get }
    
    func encoded() throws -> Data
}

extension CVUEncodable {
    public var dateFormat: String {
        return "yyyy-MM-dd'T'HH:mm:ss'Z'"
    }
    
    public func encoded() throws -> Data {
        let encoder = JSONEncoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        
        do {
            return try encoder.encode(self)
        } catch {
            print("Error: " + error.localizedDescription)
            throw CVUErrorType.jsonSerializationFailed
        }
    }
}

extension Array : CVUEncodable
where Element : CVUEncodable {
    public func encoded() throws -> Data {
        let encoder = JSONEncoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        
        do {
            return try encoder.encode(self)
        } catch {
            print("Error: " + error.localizedDescription)
            throw CVUErrorType.jsonSerializationFailed
        }
    }
}
