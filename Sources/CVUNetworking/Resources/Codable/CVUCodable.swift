//
//  CVUCodable.swift
//  Evrlink V2
//
//  Created by Razvan Balint on 02/12/2018.
//  Copyright © 2018 Razvan Balint. All rights reserved.
//

import Foundation

public protocol CVUCodable: CVUEncodable & CVUDecodable {
}
