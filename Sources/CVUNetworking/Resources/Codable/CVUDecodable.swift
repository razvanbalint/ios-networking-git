//
//  CVUDecodable.swift
//  Healthcare
//
//  Created by Razvan Balint on 03/04/2020.
//  Copyright © 2020 Razvan Balint. All rights reserved.
//

import Foundation

public protocol CVUDecodable: Decodable {
    static var dateFormat: String { get }
    
    static func decoded(from data: Data) throws -> Self
}

extension CVUDecodable {
    public static var dateFormat: String {
        return "yyyy-MM-dd'T'HH:mm:ss'Z'"
    }
    
    public static func decoded(from data: Data) throws -> Self {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        
        do {
            return try decoder.decode(Self.self, from: data)
        } catch {
            print("Error: " + error.localizedDescription)
            throw CVUErrorType.jsonMappingFailed
        }
    }
}

extension Array : CVUDecodable
where Element : CVUDecodable {
    public static func decoded(from data: Data) throws -> Array {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        
        do {
            return try decoder.decode(self.self, from: data)
        } catch {
            print("Error: " + error.localizedDescription)
            throw CVUErrorType.jsonMappingFailed
        }
    }
}
