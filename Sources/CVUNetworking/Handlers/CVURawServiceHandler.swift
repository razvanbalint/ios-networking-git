//
//  CVURawServiceHandler.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 06/12/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

struct CVURawServiceHandler: RawServiceHandler {
    
    private var resourceUrl: String
    private var requestService: RequestService
    private var headers = [String: String]()
    
    internal init(resourceUrl: String) {
        self.resourceUrl = resourceUrl
        self.requestService = RequestService()
    }
    
    mutating func addHeader(_ header: String, value: String) {
        headers[header] = value
    }
    
    func query(endpoint: String?, _ params: [String: String]? = nil, callback: @escaping (CVURawResponse) -> Void) {
        let url = resourceUrl + (endpoint ?? "") + RequestService.getURLEncoded(params)
        resourceRequest(url, method: .get, postData: nil, callback: callback)
    }
    
    func get(endpoint: String?, _ id: String, params: [String: String]? = nil, callback: @escaping (CVURawResponse) -> Void) {
        let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id + RequestService.getURLEncoded(params)
        resourceRequest(targetUrlWithParam, method: .get, postData: nil, callback: callback)
    }
    
    func post<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource {
        do {
            let jsonObj = try resource.encoded()
            resourceRequest(resourceUrl + (endpoint ?? ""), method: .post, postData: jsonObj, callback: callback)
        } catch {
            let response = CVURawResponse.localError(.jsonSerializationFailed)
            callback(response)
        }
    }
    
    func post(endpoint: String?, _ resource: ExpressibleByNilLiteral?, callback: @escaping (CVURawResponse) -> Void) {
        resourceRequest(resourceUrl + (endpoint ?? ""), method: .post, postData: nil, callback: callback)
    }
    
    func put<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource {
        if let id = resource.idValue {
            let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
            do {
                let jsonObj = try resource.encoded()
                resourceRequest(targetUrlWithParam, method: .put, postData: jsonObj, callback: callback)
            } catch {
                let response = CVURawResponse.localError(.jsonSerializationFailed)
                callback(response)
            }
        } else {
            let response = CVURawResponse.localError(.invalidResourceId)
            callback(response)
        }
    }
    
    func patch<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource {
        if let id = resource.idValue {
            let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
            do {
                let jsonObj = try resource.encoded()
                resourceRequest(targetUrlWithParam, method: .patch, postData: jsonObj, callback: callback)
            } catch {
                let response = CVURawResponse.localError(.jsonMappingFailed)
                callback(response)
            }
        } else {
            let response = CVURawResponse.localError(.invalidResourceId)
            callback(response)
        }
    }
    
    func delete(endpoint: String?, _ id: String, callback: @escaping (CVURawResponse) -> Void) {
        let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
        resourceRequest(targetUrlWithParam, method: .delete, postData: nil, callback: callback)
    }
    
    func resourceRequest(_ url: String, method: RequestMethod, postData: Data? = nil, callback: @escaping (CVURawResponse) -> Void) {
        requestService.call(url, method: method, header: headers, jsonData: postData, callback: callback)
    }
    
    func multipartPost(_ url: String, method: RequestMethod, postData: Data, filename: String, callback: @escaping (CVURawResponse) -> Void) {
        requestService.upload(url, method: method, header: headers, data: postData, filename: filename, callback: callback)
    }
}
