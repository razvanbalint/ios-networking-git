//
//  RawServiceHandlerProtocol.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 06/12/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

public protocol RawServiceHandler {
    mutating func addHeader(_ header: String, value: String)
    
    func query(endpoint: String?, _ params: [String: String]?, callback: @escaping (CVURawResponse) -> Void)
    
    func get(endpoint: String?, _ id: String, params: [String: String]?, callback: @escaping (CVURawResponse) -> Void)
    
    func delete(endpoint: String?, _ id: String, callback: @escaping (CVURawResponse) -> Void)
    
    func post<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource
    
    func post(endpoint: String?, _ resource: ExpressibleByNilLiteral?, callback: @escaping (CVURawResponse) -> Void)
    
    func put<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource
    
    func patch<T>(endpoint: String?, _ resource: T, callback: @escaping (CVURawResponse) -> Void) where T : CVUResource
    
    func resourceRequest(_ url: String, method: RequestMethod, postData: Data?, callback: @escaping (CVURawResponse) -> Void)
    
    func multipartPost(_ url: String, method: RequestMethod, postData: Data, filename: String, callback: @escaping (CVURawResponse) -> Void)
}
