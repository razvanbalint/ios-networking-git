//
//  ServiceHandlerProtocol.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

public protocol ServiceHandler {
    var resourceUrl: String { get }
    
    var raw: RawServiceHandler { get }
    
    mutating func addHeader(_ header: String, value: String)
    
    func query<T, E>(endpoint: String?, _ params: [String: String]?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol
    
    func get<T, E>(endpoint: String?, _ id: String, params: [String: String]?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol
    
    func delete<T, E>(endpoint: String?, _ id: String, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol
    
    func post<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUEncodable, T2 : CVUDecodable, E : CVUErrorProtocol
    
    func post<T, E>(endpoint: String?, _ resource: ExpressibleByNilLiteral?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol
    
    func put<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUResource, T2: CVUDecodable, E : CVUErrorProtocol
    
    func patch<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUResource, T2 : CVUDecodable, E : CVUErrorProtocol
    
    func resourceRequest<T, E>(_ url: String, method: RequestMethod, postData: Data?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol
}
