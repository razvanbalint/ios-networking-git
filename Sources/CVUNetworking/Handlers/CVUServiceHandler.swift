//
//  CVUServiceHandler.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

struct CVUServiceHandler: ServiceHandler {
    
    let resourceUrl: String
    private var requestService: RequestService
    private var headers = [String: String]()
    private (set) var raw: RawServiceHandler
    
    init(resourceUrl: String) {
        self.resourceUrl = resourceUrl
        self.requestService = RequestService()
        self.raw = CVURawServiceHandler(resourceUrl: resourceUrl)
    }
    
    mutating func addHeader(_ header: String, value: String) {
        headers[header] = value
        raw.addHeader(header, value: value)
    }
    
    func query<T, E>(endpoint: String?, _ params: [String: String]?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        let url = resourceUrl + (endpoint ?? "") + RequestService.getURLEncoded(params)
        self.resourceRequest(url, method: .get, postData: nil, callback: callback)
    }
    
    func get<T, E>(endpoint: String?, _ id: String, params: [String : String]? = nil, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id + RequestService.getURLEncoded(params)
        resourceRequest(targetUrlWithParam, method: .get, postData: nil, callback: callback)
    }
    
    func post<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUEncodable, T2 : CVUDecodable, E : CVUErrorProtocol {
        do {
            let jsonObj = try resource.encoded()
            resourceRequest(resourceUrl + (endpoint ?? ""), method: .post, postData: jsonObj, callback: callback)
        } catch {
            let raw = CVURawResponse.localError(.jsonSerializationFailed)
            self.handleRawResponse(response: raw, callback: callback)
        }
    }
    
    func post<T, E>(endpoint: String?, _ resource: ExpressibleByNilLiteral?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        resourceRequest(resourceUrl + (endpoint ?? ""), method: .post, postData: nil, callback: callback)
    }
    
    func put<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUResource, T2 : CVUDecodable, E : CVUErrorProtocol {
        if let id = resource.idValue {
            let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
            do {
                let jsonObj = try resource.encoded()
                resourceRequest(targetUrlWithParam, method: .put, postData: jsonObj, callback: callback)
            } catch {
                let raw = CVURawResponse.localError(.jsonSerializationFailed)
                self.handleRawResponse(response: raw, callback: callback)
            }
        } else {
            let raw = CVURawResponse.localError(.invalidResourceId)
            self.handleRawResponse(response: raw, callback: callback)
        }
    }
    
    func patch<T1, T2, E>(endpoint: String?, _ resource: T1, callback: @escaping (CVUResponse<T2, E>) -> Void) where T1 : CVUResource, T2 : CVUDecodable, E : CVUErrorProtocol {
        if let id = resource.idValue {
            let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
            do {
                let jsonObj = try resource.encoded()
                resourceRequest(targetUrlWithParam, method: .patch, postData: jsonObj, callback: callback)
            } catch {
                let raw = CVURawResponse.localError(.jsonSerializationFailed)
                self.handleRawResponse(response: raw, callback: callback)
            }
        } else {
            let raw = CVURawResponse.localError(.invalidResourceId)
            self.handleRawResponse(response: raw, callback: callback)
        }
    }
    
    func delete<T, E>(endpoint: String?, _ id: String, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        let targetUrlWithParam = resourceUrl + (endpoint ?? "") + "/" + id
        resourceRequest(targetUrlWithParam, method: .delete, postData: nil, callback: callback)
    }
    
    func resourceRequest<T, E>(_ url: String, method: RequestMethod, postData: Data?, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        raw.resourceRequest(url, method: method, postData: postData) { (response: CVURawResponse) in
            self.handleRawResponse(response: response, callback: callback)
        }
    }
    
    private func handleRawResponse<T, E>(response: CVURawResponse, callback: @escaping (CVUResponse<T, E>) -> Void) where T : CVUDecodable, E : CVUErrorProtocol {
        var decodedResponse: CVUResponse<T, E>
        
        switch response {
        case .success(let statusCode, let data, let headers):
            let data: T? = decode(data: data)
            decodedResponse = CVUResponse.success(statusCode: statusCode, data: data, headers: headers)
        case .error(let statusCode, let error, let headers):
            let error: E? = decode(data: error)
            decodedResponse = CVUResponse<T, E>.error(statusCode: statusCode, error: error, headers: headers)
        }
        
        callback(decodedResponse)
    }
    
    private func decode<T>(data: Data?) -> T? where T : CVUDecodable {
        guard let data = data else { return nil }
        do {
            return try T.decoded(from: data)
        } catch {
            print("Error decoding: " + error.localizedDescription)
            return nil
        }
    }
}
