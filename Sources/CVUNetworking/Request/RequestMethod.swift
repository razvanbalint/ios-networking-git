//
//  RequestMethod.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

public enum RequestMethod: String {
    case get = "get"
    case post = "post"
    case put = "put"
    case patch = "patch"
    case delete = "delete"
}
