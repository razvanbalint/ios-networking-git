//
//  RequestService.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 23/10/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation
import SystemConfiguration


public class RequestService: NSObject {
    public class func getURLEncoded(_ dictionary: [String: String]?) -> String {
        guard let dictionary = dictionary, !dictionary.isEmpty else { return "" }
        
        var urlEncoded = "?"
        dictionary.forEach { (key, value) in
            urlEncoded += "\(key)=\(value)&"
        }
        urlEncoded.remove(at: urlEncoded.index(before: urlEncoded.endIndex))
        return urlEncoded
    }
    
    public class func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        guard SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) else { return false }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    override init() {
    }
    
    internal func call(_ url: String, method: RequestMethod, header: Dictionary<String, String>?, jsonData: Data?, callback: @escaping (CVURawResponse) -> Void) {
        guard let escapedString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let urlUnwrapped = URL(string: escapedString) else { return }
        var request = URLRequest(url: urlUnwrapped)
        
        header?.forEach({ (key, value) in
            request.addValue(value, forHTTPHeaderField: key)
        })
        
        if request.value(forHTTPHeaderField: "Content-Type") == nil {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        let contentLength = (jsonData?.count ?? 0).description
        request.addValue(contentLength, forHTTPHeaderField: "Content-Length")
        
        request.httpMethod = method.rawValue
        request.httpBody = jsonData
        
        print("Hitting URL: [" + method.rawValue + "] " + url)
        print("Headers: \(String(describing: request.allHTTPHeaderFields))")
        print("Body: \(String(decoding: jsonData ?? Data(), as: UTF8.self))")
        print("Current thread: \(Thread.isMainThread)")
        
        DispatchQueue.global().async {
            self.executeTask(with: request, callback: callback)
        }
    }
    
    internal func upload(_ urlString: String, method: RequestMethod, header: Dictionary<String, String>?, data: Data, filename: String, callback: @escaping (CVURawResponse) -> Void) {
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let name = "file"
        let mimetype = "application/pdf"
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method.rawValue
        
        var body = NSMutableData()
        func appendString(_ string: String) {
            guard let data = string.data(using: .utf8, allowLossyConversion: false) else { return }
            body.append(data)
        }
        
        appendString("--\(boundary)\r\n")
        appendString("Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(filename)\"\r\n")
        appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(data)
        appendString("\r\n")
        appendString("--\(boundary)--\r\n")
        
        request.httpBody = body as Data
        header?.forEach { (key, value) in
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        DispatchQueue.global().async {
            self.executeTask(with: request, callback: callback)
        }
    }
    
    private func executeTask(with request: URLRequest, callback: @escaping (CVURawResponse) -> Void) {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 180.0
        sessionConfig.timeoutIntervalForResource = 300.0

        URLSession(configuration: sessionConfig).dataTask(with: request) { data, response, error in
            guard let response = response as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    let errorType = RequestService.connectedToNetwork() ? CVUErrorType.noResponse : CVUErrorType.noInternetConnection
                    let error = CVUError(type: errorType)
                    let response = CVURawResponse.error(statusCode: nil, error: try? JSONEncoder().encode(error), headers: nil)
                    callback(response)
                }
                return
            }
            
            if let url = response.url {
                print("Request URL: " + url.absoluteString)
            }
            print("Response headers: " + String(describing: response.allHeaderFields))
            if let data = data, let debugData = String(data: data, encoding: .utf8) {
                print("Response body: " + debugData)
            }
            
            var cvuResponse: CVURawResponse
            
            if 200...299 ~= response.statusCode {
                cvuResponse = CVURawResponse.success(statusCode: response.statusCode, data: data, headers: response.allHeaderFields)
            } else if let data = data {
                cvuResponse = CVURawResponse.error(statusCode: response.statusCode, error: data, headers: response.allHeaderFields)
            } else {
                let message = error?.localizedDescription ?? CVUErrorType.unknown.localizedDescription
                let error = CVUError(message: message)
                cvuResponse = CVURawResponse.error(statusCode: response.statusCode, error: try? JSONEncoder().encode(error), headers: response.allHeaderFields)
            }
            
            DispatchQueue.main.async {
                callback(cvuResponse)
            }
        }.resume()
    }
}
