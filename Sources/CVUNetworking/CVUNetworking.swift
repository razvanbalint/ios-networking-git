//
//  CVUNetworking.swift
//  CVUNetworking
//
//  Created by Razvan Balint on 07/12/2017.
//  Copyright © 2017 Razvan Balint. All rights reserved.
//

import Foundation

public struct CVUNetworking {

    private init() {
    }

    public static func new(resourceUrl: String) -> ServiceHandler {
        return CVUServiceHandler(resourceUrl: resourceUrl)
    }
}
